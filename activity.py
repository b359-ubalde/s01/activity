# Item no. 2
name = "Edward"
age = 24
occupation = "Front End Developer"
movie = "Klaus"
rating = 95.5

print(f"I am {name} and I am {age} years old. I work as a {occupation}, and my rating {movie} is {rating}%.")

# Item no. 3
num1, num2, num3 = 2, 3, 4
product = num1*num2
print(f"Product of {num1} and {num2} is: " + str(product))
print(f"Is {num1} less than {num3}? The Answer is:")
print(num1 < num3)
num2 += num3
print("Adding the value of num3 to num2, the new value of num2 is: " + str(num2))
